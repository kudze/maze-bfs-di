<?php

$M; //Labirinto matmenys
$N;
$LAB; //Labirintas
$LAB_COPY;
$UZD = 0;
$NAUJA = 0;
$FRONTAI = [];
$PATH_STACK = [];

const PRODUCTIONS = [
    ['x' => -1, 'y' => 0],
    ['x' => 0, 'y' => -1],
    ['x' => 1, 'y' => 0],
    ['x' => 0, 'y' => 1]
];

function KOpposite(int $K): int
{
    switch ($K) {
        case 0:
            return 2;
        case 1:
            return 3;
        case 2:
            return 0;
        case 3:
            return 1;
    }

    throw new InvalidArgumentException("$K must be in range [0-3]");
}

function ATGAL(int $U, int $V): void
{
    global $M, $N, $LAB, $LAB_COPY, $PATH_STACK;

    $LAB[$U][$V] = $LAB_COPY[$U][$V];
    $K = 4;

    do {
        $K--;
        $UU = $U + PRODUCTIONS[$K]['x'];
        $VV = $V + PRODUCTIONS[$K]['y'];

        if (0 > $UU || $UU >= $M || 0 > $VV || $VV >= $N)
            continue;

        if ($LAB_COPY[$UU][$VV] != $LAB_COPY[$U][$V] - 1)
            continue;

        $PATH_STACK = [KOpposite($K), ...$PATH_STACK];
        $LAB[$UU][$VV] = $LAB_COPY[$UU][$VV];
        $U = $UU;
        $V = $VV;
        $K = 4;
    } while ($LAB_COPY[$U][$V] != 2);
}

function BANGA(int $X, int $Y): ?array
{
    global $M, $N, $UZD, $NAUJA, $FRONTAI, $LAB, $LAB_COPY;

    $NAUJA_toPrint = $NAUJA + 1;
    $X_toPrint = $X + 1;
    $Y_toPrint = $Y + 1;
    echo "BANGA 0, žymė L=\"2\". Pradinė padėtis X=$X_toPrint, Y=$Y_toPrint, NAUJA=$NAUJA_toPrint\n\n";

    if ($X == 0 || $X == $M - 1 || $Y == 0 || $Y == $N - 1)
        return [$X, $Y];

    $lastNextMark = null;
    do {
        $X = $FRONTAI[$UZD]['x'];
        $Y = $FRONTAI[$UZD]['y'];
        $K = 0;

        $nextMark = $LAB_COPY[$X][$Y] + 1;
        if ($lastNextMark != $nextMark) {
            $bangaNr = $nextMark - 2;
            echo "BANGA $bangaNr, žymė L=\"$nextMark\"\n";
            $lastNextMark = $nextMark;
        }

        $UZD_toPrint = $UZD + 1;
        $X_toPrint = $X + 1;
        $Y_toPrint = $Y + 1;
        echo "    Uždaroma UZD=$UZD_toPrint, X=$X_toPrint, Y=$Y_toPrint.\n";

        foreach (PRODUCTIONS as $production) {
            $K++;
            $U = $X + $production['x'];
            $V = $Y + $production['y'];

            $U_toPrint = $U + 1;
            $V_toPrint = $V + 1;
            echo "        R$K. X=$U_toPrint, Y=$V_toPrint. ";

            if ($LAB_COPY[$U][$V] === 1) {
                echo "Siena.\n";
                continue;
            }

            if ($LAB_COPY[$U][$V] !== 0) {
                echo "UŽDARYTA arba ATIDARYTA.\n";
                continue;
            }

            $NAUJA++;
            $FRONTAI[$NAUJA] = [
                'x' => $U,
                'y' => $V
            ];

            $NAUJA_toPrint = $NAUJA + 1;
            echo "Laisva. NAUJA=$NAUJA_toPrint.";

            $LAB_COPY[$U][$V] = $nextMark;
            if ($U == 0 || $U == $M - 1 || $V == 0 || $V == $N - 1) {
                echo " Terminalinė.\n\n";
                return [$U, $V];
            }

            echo "\n";
        }
        echo "\n";

        $UZD++;
    } while ($UZD <= $NAUJA);

    return null;
}

function initFromFile(string $file): array
{
    global $M, $N, $LAB, $LAB_COPY, $FRONTAI;

    $contents = file_get_contents($file);
    $lines = explode("\n", $contents);
    $startPos = array_pop($lines);

    $N = count($lines);
    $n = 0;

    $LAB = array_fill(0, $N, []);

    foreach ($lines as $line) {
        $cells = array_map(fn(string $cell) => (int) $cell, explode(' ', $line));

        $M = count($cells);
        $m = 0;

        foreach ($cells as $cell) {
            $LAB[$m][$n] = $cell;
            $m++;
        }

        $n++;
    }

    [$startX, $startY] = array_map(fn(string $cell) => ((int) $cell - 1), explode(' ', $startPos));

    $FRONTAI = [['x' => $startX, 'y' => $startY]];
    $LAB[$startX][$startY] = 2;
    $LAB_COPY = $LAB;

    return [$startX, $startY];
}

function printTable(array $LAB): void
{
    global $M, $N;

    echo "    Y, V\n";
    echo "    ^\n";

    for ($Y = $N - 1; $Y >= 0; $Y--) {
        $yToPrint = str_pad($Y + 1, 3, ' ', STR_PAD_LEFT);

        echo "$yToPrint | ";

        for ($X = 0; $X < $M; $X++)
            echo str_pad($LAB[$X][$Y], 3, ' ', STR_PAD_LEFT);

        echo "\n";
    }

    $line = str_repeat('-', ($M + 1) * 3 + 1);
    echo "    $line> X, U\n";
    echo "      ";
    for ($X = 0; $X < $M; $X++)
        echo str_pad($X + 1, 3, ' ', STR_PAD_LEFT);
    echo "\n\n";
}

function printMaze(): void
{
    global $startX, $startY, $LAB;

    echo "PIRMA DALIS. Duomenys\n";
    echo "  1.1. Labirintas\n\n";

    printTable($LAB);

    $startXToPrint = $startX + 1;
    $startYToPrint = $startY + 1;
    echo "  1.2 Pradinė padėtis: X=$startXToPrint, Y=$startYToPrint, L=2.\n\n";

    echo "ANTRA DALIS. Vykdymas\n\n";
}

function printResults(bool $yra, int $startX, int $startY): void
{
    global $PATH_STACK, $LAB_COPY;

    echo "\nTREČIA DALIS. Rezultatai\n\n";
    if (!$yra) {
        echo "  3.1. Kelias nerastas.\n";
        return;
    }

    echo "  3.1. Kelias rastas.\n\n";
    echo "  3.2. Kelias grafiškai:\n\n";

    echo "LABCOPY\n";
    printTable($LAB_COPY);

    $path = join(", ", array_map(fn(int $path) => 'R' . ($path + 1), $PATH_STACK));
    echo "  3.3. Kelias taisyklėmis: $path.\n\n";

    $pathCoordinates = array_reduce(
        $PATH_STACK,
        function (array $carry, int $key) {
            $lastPos = $carry[array_key_last($carry)];
            $carry[] = [
                'x' => $lastPos['x'] + PRODUCTIONS[$key]['x'],
                'y' => $lastPos['y'] + PRODUCTIONS[$key]['y'],
            ];

            return $carry;
        },
        [['x' => $startX, 'y' => $startY]]
    );

    $pathCoordinates = join(', ', array_map(fn(array $coords) => '[X=' . $coords['x'] + 1 . ',Y=' . $coords['y'] + 1 . ']', $pathCoordinates));
    echo "  3.4. Kelias viršūnėmis: $pathCoordinates.\n\n";
}

if ($argc <= 1)
    exit("Naudojimas: php maze_bfs.php <failas>\n* Failas - labirinto failas\n");

if (!file_exists($argv[1]))
    exit("Ivesties failas: \"{$argv[1]}\" nebuvo rastas!\n");

[$startX, $startY] = initFromFile($argv[1]);
printMaze();

$result = BANGA($startX, $startY);

if ($result === null)
    printResults(false, 0, 0);

echo "Kelias iš labirinto egzistuoja!\n";
[$endX, $endY] = $result;
ATGAL($endX, $endY);
printResults(true, $startX, $startY);